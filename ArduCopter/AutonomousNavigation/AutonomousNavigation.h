#include "../Copter.h"

using namespace std;

// Total of 5 moves
const int MOVE_SIZE = 5;

float target_distance = 0.35;       // Target distance to reach before considering that we have succeeded in reaching this wall.
float right_target_distance = 0.25; // Special case for the right wall, must be closer since we don't want to hit the front wall.
float hover_distance = 0.5;         // Distance to hover from the walls (centered between them)

enum dir
{
    takeoff,
    left,
    right,
    forward,
    backward
};

struct Move
{
    dir move_dir; // direction of movement
};

Move l = {dir::left};
Move r = {dir::right};
Move u = {dir::forward};
Move d = {dir::backward};
Move t = {dir::takeoff};

// Moves in order: takeoff, move left, move up, move right, and move up
Move moves[MOVE_SIZE] = {
    t,
    l,
    u,
    r,
    u,
};

int cur_move_index = 0;

// Function to reset values and initialize variables from the parameters in Mission Planner
void nav_init(float target_dist, float right_target_dist, float hover_dist)
{
    cur_move_index = 0;
    hover_distance = hover_dist;
    right_target_distance = right_target_dist;
    target_distance = target_dist;
}

// Sets pitch such that it will hover between the forward and backward walls.
// Finds the closer of the two walls, and uses that as the target.
float pitch_fb_walls(float dist_forward, float dist_backward)
{

    // Both walls are within range, so hover between them.
    if (dist_forward < 2 && dist_backward < 2)
    {
        return dist_backward - dist_forward;
    }
    // One wall is out of range, so hover close to that wall.
    else
    {

        // Hover around forward wall, since its closer.
        if (dist_forward < dist_backward)
        {
            return hover_distance - dist_forward;
        }
        // Hover around backward wall, since its closer
        else
        {
            return -(hover_distance - dist_backward);
        }
    }
}

// Sets roll such that it will hover between the left and right walls.
// Finds the closer of the two walls, and uses that as the target.
float roll_rl_walls(float dist_left, float dist_right)
{

    // Both walls are within range, so hover between them.
    if (dist_left < 2 && dist_right < 2)
    {
        return dist_left - dist_right;
    }
    else
    {

        // Hover around left wall, since its closer.
        if (dist_left < dist_right)
        {
            return -(dist_left - hover_distance);
        }
        // Hover around right wall, since its closer
        else
        {
            return dist_right - hover_distance;
        }
    }
}

// Returns true to continue flying, false to land
bool navigate(float &pitch_pid_error, float &roll_pid_error, float dist_left, float dist_right, float dist_forward, float dist_backward, float rangefinder, string &debug)
{
    float cur_move_distance = 0;

    // If the sensor isn't working properly (very small readings), then set it high
    if (dist_backward < .1)
    {
        dist_backward = 5;
    }
    if (dist_forward < .1)
    {
        dist_forward = 5;
    }
    if (dist_left < .1)
    {
        dist_left = 5;
    }
    if (dist_right < .1)
    {
        dist_right = 5;
    }

    switch (moves[cur_move_index].move_dir)
    {
    // Takeoff, wait until we are .28m above the ground before moving to the next step
    case dir::takeoff:
        cur_move_distance = 10;
        if (rangefinder > 0.28)
        {
            cur_move_index++;
        }
        debug = "Takeoff";
        pitch_pid_error = pitch_fb_walls(dist_forward, dist_backward);
        roll_pid_error = roll_rl_walls(dist_left, dist_right);
        break;
    // Cases for moving Left, Right, Forward, and Backward
    case dir::left:
        cur_move_distance = dist_left;
        debug = "Left";
        roll_pid_error = -(dist_left - target_distance);               // Should roll to the left, which is negative
        pitch_pid_error = pitch_fb_walls(dist_forward, dist_backward); // maintain distance between two walls
        break;
    case dir::right:
        cur_move_distance = dist_right;
        debug = "Right";
        roll_pid_error = dist_right - right_target_distance; // Should roll to the right, which is positive
        pitch_pid_error = pitch_fb_walls(dist_forward, dist_backward);
        break;
    case dir::forward:
        cur_move_distance = dist_forward;
        debug = "Forward";
        pitch_pid_error = target_distance - dist_forward; // Should pitch forward, which is negative
        roll_pid_error = roll_rl_walls(dist_left, dist_right);
        break;
    case dir::backward:
        cur_move_distance = dist_backward;
        debug = "Backward";
        pitch_pid_error = -(target_distance - dist_backward); // Should pitch backward, which is positive
        roll_pid_error = roll_rl_walls(dist_left, dist_right);
        break;
    }

    static int good_counter = 0;

    // The target is within .1 meters of the target_distance, so move on to the next move.
    if (cur_move_distance < target_distance + .1)
    {
        //if (good_counter > 400)
        //{
        debug += "Next Move";
        cur_move_index++;
        // Ran out of moves to make, so must have reached end of the maze.
        if (cur_move_index >= MOVE_SIZE)
        {
            debug = "Land - Out of Moves";
            return false;
        }
        //  good_counter = 0;
        //}
        //good_counter++;
    }

    return true;
}
