// Navigtaion Code from Jacob

bool Copter::autonomous_controller(float &target_climb_rate, float &target_roll, float &target_pitch, float &target_yaw_rate)
{
    // get downward facing sensor reading in meters
    float rangefinder_alt = (float)rangefinder_state.alt_cm / 100.0f;

    // get horizontal sensor readings in meters
    float dist_forward, dist_right, dist_backward, dist_left;
    g2.proximity.get_horizontal_distance(0, dist_forward);
    g2.proximity.get_horizontal_distance(90, dist_right);
    g2.proximity.get_horizontal_distance(180, dist_backward);
    g2.proximity.get_horizontal_distance(270, dist_left);

    // set desired climb rate in centimeters per second
    target_climb_rate = 0.0f;

    // set desired roll and pitch in centi-degrees
    //target_pitch = 0.0f;
    //target_roll = 0.0f;

    // Buffer
    double buffer = .3;
    double buffer_angle_factor = 1;

    if (dist_right < buffer || dist_left < buffer || dist_forward < buffer || dist_backward < buffer)
    {
        if (dist_right < buffer)
        {
            // As the drone goes further past the buffer, the angle grows. Grows at increased rate by increasing angle_factor.
            target_roll = 100.0f * (dist_right - buffer) * angle_factor;
        }
        else if (dist_left < buffer)
        {
            target_roll = 100.0f * (buffer - dist_left) * angle_factor;
        }
        if (dist_backward < buffer)
        {
            target_pitch = 100.0f * (buffer - dist_backward) * angle_factor;
        }
        else if (dist_forward < buffer)
        {
            target_pitch = 100.0f * (dist_forward - buffer) * angle_factor;
        }
    }

    // Move through Maze
    double start_slowdown = 1;
    // target_wall_dist should be larger than buffer
    double target_wall_dist = .4;
    // Standard_angle is angle drone flies to move throught the maze
    double standard_angle = 10;

    else()
    {
        static int path = 0;

        // Moving Left
        if (path = 0)
        {
            target_pitch = 0;
            if (dist_left > start_slowdown)
            {
                target_roll = -standard_angle;
            }
            else if (dist_left <= start_slowdown && dist_left > target_wall_dist)
            {
                // Decreases angle at constant rate with distance to target_wall_dist. Gives zero at target_wall_dist and gives standard_angle at start_slowdown
                target_roll = 100.0f * -(target_wall_dist - dist_left) * standard_angle / (target_wall_dist - start_slowdown);
            }
            // Not just else in case not reading a distance messes with this
            else if (dist_left <= target_wall_dist)
            {
                path = 1;
            }
        }

        // Moving Up
        if (path = 1)
        {
            target_roll = 0;
            if (dist_forward > start_slowdown)
            {
                target_pitch = standard_angle;
            }
            else if (dist_forward <= start_slowdown && dist_forward > target_wall_dist)
            {
                target_pitch = 100.0f * (target_wall_dist - dist_forward) * standard_angle / (target_wall_dist - start_slowdown);
            }
            else if (dist_forward <= target_wall_dist)
            {
                path = 2;
            }
        }

        // Moving right
        if (path = 2)
        {
            target_pitch = 0;
            if (dist_right > start_slowdown)
            {
                target_roll = standard_angle;
            }
            else if (dist_right <= start_slowdown && dist_right > target_wall_dist)
            {
                target_roll = 100.0f * (target_wall_dist - dist_right) * standard_angle / (target_wall_dist - start_slowdown);
            }
            else if (dist_right <= target_wall_dist)
            {
                path = 3;
            }
        }

        // Moving Up
        if (path = 3)
        {
            target_roll = 0;
            if (dist_forward > start_slowdown)
            {
                target_pitch = standard_angle;
            }
            else if (dist_forward <= start_slowdown && dist_forward > target_wall_dist)
            {
                target_pitch = 100.0f * (target_wall_dist - dist_forward) * standard_angle / (target_wall_dist - start_slowdown);
            }
            else if (dist_forward <= target_wall_dist)
            {
                //////////////////////////// Land Command Here ////////////////////////////////;
            }
        }
    }

    // set desired yaw rate in centi-degrees per second (set to zero to hold constant heading)
    target_yaw_rate = 0.0f;
    static int counter = 0;
    if (counter++ > 400)
    {
        gcs_send_text(MAV_SEVERITY_INFO, "Autonomous flight mode for X");
        counter = 0;
    }

    return true;
}