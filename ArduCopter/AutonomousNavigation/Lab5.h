// Navigation code from Lab 5

bool Copter::autonomous_controller(float &target_climb_rate, float &target_roll, float &target_pitch, float &target_yaw_rate)
{
    // get downward facing sensor reading in meters
    float rangefinder_alt = (float)rangefinder_state.alt_cm / 100.0f;

    // get horizontal sensor readings in meters
    float dist_forward, dist_right, dist_backward, dist_left;
    g2.proximity.get_horizontal_distance(0, dist_forward);
    g2.proximity.get_horizontal_distance(90, dist_right);
    g2.proximity.get_horizontal_distance(180, dist_backward);
    g2.proximity.get_horizontal_distance(270, dist_left);

    // set desired climb rate in centimeters per second
    target_climb_rate = 0.0f;
    target_yaw_rate = 0.0f;

    // set desired roll and pitch in centi-degrees
    //target_pitch = 0.0f;
    //target_roll = 0.0f;

    g.pid_roll.set_input_filter_all(dist_right - dist_left);
    target_roll = 100.0f * g.pid_roll.get_pid();

    g.pid_pitch.set_input_filter_all(g.e100_param1 - dist_forward);
    target_pitch = 100.0f * g.pid_pitch.get_pid();

    static int counter = 0;
    if (counter++ > 400)
    {
        gcs_send_text(MAV_SEVERITY_INFO, "Autonomous flight mode for X");
        counter = 0;
    }

    return true;
}
